# Setting up Portainer Docker on CentOS host

1. For the control node, you would want to set up the **Portainer Server** container by following the instructions provided [here](setting-up-portainer-server-docker-on-centos-host/README.md).
2. For the worker nodes, you would want to set up the **Portainer Agent** containers by following the instructions provided [here](setting-up-portainer-agent-docker-on-centos-host/README.md).
