# Setting up Portainer Agent Docker on CentOS host

> Reference : https://docs.portainer.io/v/ce-2.9/start/install/agent/docker/linux

1. Clone the repo to your local storage and make the `setting-up-portainer-agent-docker-on-centos-host` directory your current working directory.  
2. Make changes to `prepbook.yml` according to your preferences to reflect the changes in `hostaddr`, `username`, `servlist`, `portagnt_hostname`, `portagnt_webp`, `portagnt_sock` and `portagnt_volm` variables.  
3. Once done, execute the following command to populate the primary playbook and inventory file.  
    ```
    ansible-playbook prepbook.yml -vvv
    ```
4. Fetch the `community.docker` collection from Ansible Galaxy by executing the following command.  
    ```
    ansible-galaxy collection install community.docker
    ```
5. Two new files would be generated as a result, so execute the following command to actually start setting up Portainer Agent Docker.  
    ```
    ansible-playbook -i register.ini trapplay.yml -vvv
    ```
6. Once the containers are configured and started, take a note of `https://<ansible_host>:<portagnt_webp>/` (or `https://192.168.0.151:9001/` if nothing was changed) as this is how Portainer Server would connect to this Portainer Agent environment.
